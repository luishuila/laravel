<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validaddatos extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            $this->validated([
                "name" => 'required|min:3',
				"email" => 'required|email|min:4',
                "documento" => 'required|numeric|min:6',
                'password' => 'required',
                "role_id" => 'required|numeric',
                "direccion"=>'required|min:5',

            ])
        ];
    }
}
