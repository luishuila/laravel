<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $users = DB::table('users')->select('*')->where('role_id',3)->get();

        return view('clients.index',compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        DB::table('users')->insert(
			[
		       "name" => $request->input('name'),
                "email" => $request->input('email'),
                "password" => $request->input(''),
                "documento" => $request->input('documento'),
               "direccion"=>$request->input('direccion'),
               "role_id"=>("3"),
				"created_at" => Carbon::now()

			]
        );
        $users =  DB::table('users')->select('*')->where('role_id',3)->get();

        return view('clients.index',compact('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datos = DB::table('users')->where('id', $id)->first();
        return view('clients.edit', compact('datos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('users')->where('id', $id)->update(
			[
		        "name" => $request->input('name'),
				"email" => $request->input('email'),
                "documento" => $request->input('documento'),
                "direccion" => $request->input('direccion'),
				"updated_at" => Carbon::now()
            ]

        );
        $users =  DB::table('users')->select('*')->where('role_id',3)->get();
        return view('clients.index',compact('users'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('users')->where('id', $id)->delete();
        $users =  DB::table('users')->where('id', $id)->first();
        return view('clients.index', compact('users'));

    }
}
