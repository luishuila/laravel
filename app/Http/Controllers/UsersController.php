<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Hash;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware(['auth']);
  }


  public function index()
  {
      //
      $users = \App\User::all();

      return view('users.index',compact('users'));
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categoria = \App\Roles::all();
        return view('users.create',compact('categoria'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
        DB::table('users')->insert(
			[
		        "name" => $request->input('name'),
				"email" => $request->input('email'),
                "documento" => $request->input('documento'),
                'password' => Hash::make( $request->input(['password'])),
                "role_id" => $request->input('role'),
                "direccion" => $request->input('direccion'),
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now(),
			]
        );
        $users = \App\User::all();
        return view('users.index', compact('users'));
    }catch(QueryException $e){
        return redirect()->back()->with('erno', '* Algo salio mal, asegurate que la informacion sea correcta antes de enviar');
}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $datos = DB::table('message')->where('id', $id)->first();
        return view('users.edit', compact('datos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        $datos = DB::table('users')->where('id', $id)->first();
        return view('users.edit', compact('datos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('users')->where('id', $id)->update(
			[
		        "name" => $request->input('name'),
				"email" => $request->input('email'),
                "documento" => $request->input('documento'),
                "direccion" => $request->input('direccion'),
				"updated_at" => Carbon::now()
            ]

        );
        $users = \App\User::all();
        return view('users.index',compact('users'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('users')->where('id', $id)->delete();
        $users = \App\User::all();
        return view('users.index', compact('users'));

    }
}
