<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title> ERROR | 404</title>

  <!-- Custom fonts for this template-->
  <link href="{{asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="shortcut icon" type="image/x-icon" />
  <!-- Custom styles for this template-->
  <link href="{{asset('css/sb-admin-2.min.css') }}" rel="stylesheet" type="text/css">

</head>

<body id="page-top">

      <div class="container" style="margin-top: 16%">
          <!-- 404 Error Text -->
          <div class="text-center">
            <div class="error mx-auto" data-text="404">404</div>
            <p class="lead text-gray-400 mb-5 text-capitalize">Pagina No encontrada</p>
            <p class="text-gray-500 mb-0  text-capitalize"><strong>oops esta pagina no fue encontrada</strong> para evitar toparte con esta pagina,  </p>
            <p class="text-gray-500 mb-0  text-capitalize">intenta no manipular la url de forma manual</p>
            <a href="{{ route('home') }} " class="text-capitalize">&larr;ir a una ruta valida</a>
          </div>
      </div>

</body>

</html>
