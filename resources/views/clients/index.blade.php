@extends('layouts.app')

@section('content')
<table class="table">
  <thead>
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Documento</th>
      <th scope="col">Direccion</th>
      <th scope="col">Correo</th>
    </tr>
  </thead>

  @foreach ( $users  as $user )

  <tbody>

      <td>{{ $user->name }}</td>
      <td> {{ $user->documento }}</td>
       <td> {{ $user->direccion }}</td>
        <td> {{ $user->email }}</td>

        <td><button type="button" class=" btn btn-success"><a  href="{{ route('clientes.show',$user->id) }}">Actualizar</a></button></td>
        <td>       <form method="POST" action="{{route('clientes.destroy',$user->id)}}">
            {!! method_field('DELETE') !!}
             {!! csrf_field() !!}
                <button >Elimina</button>
              </form></td>

 </tbody>
  @endforeach


</table>

@stop
